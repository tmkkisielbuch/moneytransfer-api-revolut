package com.moneytransfer.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.client.Entity;

import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.mockito.Mockito;

import com.moneytransfer.controller.mapper.AccountControllerModelAccountModelMapper;
import com.moneytransfer.controller.mapper.UserControllerModelListUserModelListMapper;
import com.moneytransfer.controller.mapper.UserControllerModelUserModelMapper;
import com.moneytransfer.controller.mapper.UserModelUserControllerModelMapper;
import com.moneytransfer.controller.model.UserControllerModel;
import com.moneytransfer.domain.implementation.UserService;
import com.moneytransfer.domain.model.UserModel;
import com.moneytransfer.infrastructure.repository.IAccountRepository;
import com.moneytransfer.infrastructure.repository.IUserRepository;

import io.dropwizard.testing.junit.ResourceTestRule;

public class UserControllerTest {

	private static final String USER_NAME1 = "omer";
	private static final String USER_NAME2 = "erkan";
	private static final String USER_EMIAL = "omer@gmail.com";
	private static final Integer USER_ID = 1;
	private static final String USER_URL = "/api/users";
	private static final String GET_USER_URL = "/api/users/omer";

	private final UserControllerModel userControllerModel1 = new UserControllerModel(USER_NAME1, USER_EMIAL);
	private final UserControllerModel userControllerModel2 = new UserControllerModel(USER_NAME2, USER_EMIAL);

	private static final UserService userService = mock(UserService.class);
	private static final IUserRepository userRepository = mock(IUserRepository.class);
	private static final UserModelUserControllerModelMapper userModelUserControllerModelMapper = mock(
			UserModelUserControllerModelMapper.class);
	private static final UserControllerModelUserModelMapper userControllerModelUserModelMapper = mock(
			UserControllerModelUserModelMapper.class);
	private static final UserControllerModelListUserModelListMapper userControllerModelListUserModelListMapper = mock(
			UserControllerModelListUserModelListMapper.class);
	private static final IAccountRepository accountRepository = mock(IAccountRepository.class);
	private static final AccountControllerModelAccountModelMapper accountControllerModelAccountModelMapper = mock(
			AccountControllerModelAccountModelMapper.class);

	@ClassRule
	public static final ResourceTestRule resources = ResourceTestRule.builder()
			.addResource(new UserController(userService, userRepository, userModelUserControllerModelMapper,
					userControllerModelUserModelMapper, userControllerModelListUserModelListMapper, accountRepository,
					accountControllerModelAccountModelMapper))
			.build();

	@Before
	public void setup() {
	}

	@After
	public void tearDown() {
		reset(userRepository);
		reset(accountRepository);

	}

	@Test
	public void testGetUser() {
		when(userService.getUser(Mockito.anyString())).thenReturn(new UserModel());

		when(userControllerModelUserModelMapper.map(Mockito.isA(UserModel.class))).thenReturn(userControllerModel1);

		assertThat(resources.client().target(GET_USER_URL).request().get(UserControllerModel.class))
				.isEqualTo(userControllerModel1);

	}

	@Test
	public void testCreateUser() {
		UserControllerModel newUser = new UserControllerModel(USER_NAME2, USER_EMIAL);
		UserControllerModel savedUser = new UserControllerModel(USER_ID, USER_NAME2, USER_EMIAL);
		UserModel userModel = new UserModel();
		userModel.setEmail(USER_EMIAL);
		userModel.setUsername(USER_NAME2);

		when(userModelUserControllerModelMapper.map(Mockito.isA(UserControllerModel.class))).thenReturn(userModel);
		when(userService.saveUser(Mockito.any(UserModel.class))).thenReturn(userModel);
		when(userControllerModelUserModelMapper.map(Mockito.isA(UserModel.class))).thenReturn(savedUser);

		assertThat(resources.client().target(USER_URL).request().post(Entity.json(newUser))
				.readEntity(UserControllerModel.class)).isEqualTo(savedUser);
	}

	@Test
	public void testGetAllUsers() {
		UserModel userServiceModel1 = new UserModel();
		userServiceModel1.setEmail(USER_EMIAL);
		userServiceModel1.setUsername(USER_NAME1);

		UserModel userServiceModel2 = new UserModel();
		userServiceModel2.setEmail(USER_EMIAL);
		userServiceModel2.setUsername(USER_NAME2);

		List<UserControllerModel> userControllerModels = Arrays.asList(userControllerModel1, userControllerModel2);
		List<UserModel> userServiceModels = Arrays.asList(userServiceModel1, userServiceModel2);

		when(userService.getAllUsers()).thenReturn(userServiceModels);
		when(userControllerModelListUserModelListMapper.map(Mockito.anyList())).thenReturn(userControllerModels);
		List<UserControllerModel> userControllerModelList = new ArrayList<>();

		assertThat(resources.client().target(USER_URL).request().get(userControllerModelList.getClass()).size())
				.isEqualTo(2);

	}
}
