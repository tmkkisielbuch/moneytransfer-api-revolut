package com.moneytransfer.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;

import javax.ws.rs.client.Entity;

import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.mockito.Mockito;

import com.moneytransfer.controller.mapper.AccountControllerModelAccountModelMapper;
import com.moneytransfer.controller.mapper.AccountControllerModelListAccountModelListMapper;
import com.moneytransfer.controller.mapper.CurrencyAmountControllerModelCurrencyAmountModelMapper;
import com.moneytransfer.controller.model.AccountControllerModel;
import com.moneytransfer.controller.model.CurrencyAmountControllerModel;
import com.moneytransfer.domain.implementation.AccountService;
import com.moneytransfer.domain.mapper.AccountModelAccountControllerModelMapper;
import com.moneytransfer.domain.model.AccountModel;
import com.moneytransfer.infrastructure.repository.IAccountRepository;

import io.dropwizard.testing.junit.ResourceTestRule;

public class AccountControllerTest {

	private static final String ACCOUNT_NAME1 = "omer";
	private static final String ACCOUNT_NAME2 = "erkan";
	private static final String CURRENCY = "EUR";
	private static final Double AMOUNT = 10.00;
	private static final Integer ACCOUNT_ID = 1;
	private static final Integer ROUND_SCALE_VALUE = 2;
	private static final String ACCOUNT_URL = "/api/accounts";
	private static final String GET_ACCOUNT_URL = "/api/accounts/1";

	private final AccountControllerModel accountController1 = new AccountControllerModel(ACCOUNT_ID, ACCOUNT_NAME1,
			new BigDecimal(AMOUNT).setScale(ROUND_SCALE_VALUE, RoundingMode.UNNECESSARY).doubleValue(),
			Currency.getInstance(CURRENCY).getCurrencyCode());

	private final AccountControllerModel accountController2 = new AccountControllerModel(ACCOUNT_ID, ACCOUNT_NAME2,
			new BigDecimal(20.00).setScale(ROUND_SCALE_VALUE, RoundingMode.UNNECESSARY).doubleValue(),
			Currency.getInstance(CURRENCY).getCurrencyCode());

	private final AccountModel accountModel = new AccountModel(ACCOUNT_ID, ACCOUNT_NAME1,
			new BigDecimal(10.00).setScale(ROUND_SCALE_VALUE, RoundingMode.UNNECESSARY).doubleValue(),
			Currency.getInstance(CURRENCY).getCurrencyCode());

	private final AccountModel accountMode2 = new AccountModel(ACCOUNT_ID, ACCOUNT_NAME2,
			new BigDecimal(20.00).setScale(ROUND_SCALE_VALUE, RoundingMode.UNNECESSARY).doubleValue(),
			Currency.getInstance(CURRENCY).getCurrencyCode());

	private final CurrencyAmountControllerModel currencyAmountControllerModel = new CurrencyAmountControllerModel(
			new BigDecimal(10.00).setScale(ROUND_SCALE_VALUE, RoundingMode.UNNECESSARY).doubleValue(),
			Currency.getInstance(CURRENCY).getCurrencyCode());

	private static final AccountService accountService = mock(AccountService.class);
	private static final IAccountRepository repository = mock(IAccountRepository.class);
	private static final AccountModelAccountControllerModelMapper accountModelAccountControllerModelMapper = mock(
			AccountModelAccountControllerModelMapper.class);
	private static final AccountControllerModelAccountModelMapper accountControllerModelAccountModelMapper = mock(
			AccountControllerModelAccountModelMapper.class);
	private static final AccountControllerModelListAccountModelListMapper accountControllerModelListAccountModelListMapper = mock(
			AccountControllerModelListAccountModelListMapper.class);
	private static final CurrencyAmountControllerModelCurrencyAmountModelMapper currencyAmountControllerModelCurrencyAmountModelMapper = mock(
			CurrencyAmountControllerModelCurrencyAmountModelMapper.class);

	@ClassRule
	public static final ResourceTestRule resources = ResourceTestRule.builder()
			.addResource(new AccountController(repository, accountService, accountModelAccountControllerModelMapper,
					accountControllerModelAccountModelMapper, accountControllerModelListAccountModelListMapper,
					currencyAmountControllerModelCurrencyAmountModelMapper))
			.addResource(new AccountService(repository)).build();

	@Before
	public void setup() {
	}

	@After
	public void tearDown() {
		reset(repository);
	}

	@Test
	public void testGetAccount() {
		when(accountService.getAccount(eq(ACCOUNT_ID))).thenReturn(new AccountModel());
		when(accountControllerModelAccountModelMapper.map(Mockito.isA(AccountModel.class)))
				.thenReturn(accountController1);

		assertThat(resources.client().target(GET_ACCOUNT_URL).request().get(AccountControllerModel.class))
				.isEqualTo(accountController1);
	}

	@Test
	public void testCreateAccount() {

		AccountControllerModel newAccount = new AccountControllerModel(ACCOUNT_NAME1,
				new BigDecimal(10.00).setScale(ROUND_SCALE_VALUE, RoundingMode.UNNECESSARY).doubleValue(),
				Currency.getInstance(CURRENCY).getCurrencyCode());

		AccountControllerModel saveControllerAccount = new AccountControllerModel(ACCOUNT_NAME1,
				new BigDecimal(10.00).setScale(ROUND_SCALE_VALUE, RoundingMode.UNNECESSARY).doubleValue(),
				Currency.getInstance(CURRENCY).getCurrencyCode());
		AccountModel savedAccount = new AccountModel(ACCOUNT_ID, ACCOUNT_NAME1,
				new BigDecimal(10.00).setScale(ROUND_SCALE_VALUE, RoundingMode.UNNECESSARY).doubleValue(),
				Currency.getInstance(CURRENCY).getCurrencyCode());

		when(accountModelAccountControllerModelMapper.map(Mockito.isA(AccountControllerModel.class)))
				.thenReturn(savedAccount);

		when(accountService.saveAccount(savedAccount)).thenReturn(savedAccount);
		when(accountControllerModelAccountModelMapper.map(Mockito.isA(AccountModel.class)))
				.thenReturn(saveControllerAccount);

		assertThat(resources.client().target(ACCOUNT_URL).request().post(Entity.json(newAccount))
				.readEntity(AccountControllerModel.class)).isEqualTo(saveControllerAccount);
	}

	@Test
	public void testgetAllAccounts() {

		final List<AccountControllerModel> accountControlelrModels = Arrays.asList(accountController1,
				accountController2);
		final List<AccountModel> accountModels = Arrays.asList(accountModel, accountMode2);

		when(accountService.getAllAccounts()).thenReturn(accountModels);
		when(accountControllerModelListAccountModelListMapper.map(Mockito.anyList()))
				.thenReturn(accountControlelrModels);
		List<AccountControllerModel> accountList = new ArrayList<>();

		assertThat(resources.client().target(ACCOUNT_URL).request().get(accountList.getClass()).size()).isEqualTo(2);
	}

}
