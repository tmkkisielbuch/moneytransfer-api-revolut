package com.moneytransfer.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.mockito.Mockito;

import com.moneytransfer.controller.mapper.MoneyTransferModelMoneyTransferControllerModelMapper;
import com.moneytransfer.controller.model.MoneyTransferControllerModel;
import com.moneytransfer.domain.implementation.MoneyTransactionService;
import com.moneytransfer.domain.model.MoneyTransferModel;
import com.moneytransfer.infrastructure.entity.AccountEntity;
import com.moneytransfer.infrastructure.repository.IAccountRepository;

import io.dropwizard.testing.junit.ResourceTestRule;

public class MoneyTransactionControllerTest {

	private static final String CURRENCY = "EUR";
	private static final Integer SOURCE_ACCOUNT_ID = 1;
	private static final Integer DESTINATIO_ACCOUNT_ID = 2;
	private static final String ACCOUNT_NAME_1 = "omer";
	private static final String ACCOUNT_NAME_2 = "erkan";
	private static final Double SOURCE_AMOUNT = 30.00;
	private static final Double TRANSFER_AMOUNT = 10.00;
	private static final Double DESTINATION_AMOUNT = 0.00;
	private static final Integer ACCOUNT_ID_1 = 1;
	private static final Integer ACCOUNT_ID_2 = 2;
	private static final Integer ROUND_SCALE_VALUE = 2;
	private static final String MONEY_TRANSFER_URL = "/api/transfers";

	private static final IAccountRepository accountRepository = mock(IAccountRepository.class);
	private static final MoneyTransactionService transferService = mock(MoneyTransactionService.class);
	private static final MoneyTransferModelMoneyTransferControllerModelMapper moneyTransferModelMoneyTransferControllerModelMapper = mock(
			MoneyTransferModelMoneyTransferControllerModelMapper.class);

	@ClassRule
	public static final ResourceTestRule resources = ResourceTestRule.builder()
			.addResource(new MoneyTransactionController(accountRepository, transferService,
					moneyTransferModelMoneyTransferControllerModelMapper))
			.build();

	@Before
	public void setup() {
	}

	@After
	public void tearDown() {
		reset(accountRepository);
	}

	@Test
	public void testTransfer() {

		MoneyTransferControllerModel transfer = new MoneyTransferControllerModel(TRANSFER_AMOUNT,
				Currency.getInstance(CURRENCY).getCurrencyCode(), SOURCE_ACCOUNT_ID, DESTINATIO_ACCOUNT_ID);

		MoneyTransferModel transferModel = new MoneyTransferModel();
		transferModel.setAmount(new BigDecimal(TRANSFER_AMOUNT)
				.setScale(ROUND_SCALE_VALUE, BigDecimal.ROUND_UNNECESSARY).doubleValue());
		transferModel.setCurrency(Currency.getInstance(CURRENCY).getCurrencyCode());
		transferModel.setDestinationAccountId(DESTINATIO_ACCOUNT_ID);
		transferModel.setSourceAccountId(SOURCE_ACCOUNT_ID);

		AccountEntity sourceAccount = new AccountEntity(ACCOUNT_ID_1, ACCOUNT_NAME_1,
				new BigDecimal(SOURCE_AMOUNT).setScale(ROUND_SCALE_VALUE, RoundingMode.UNNECESSARY).doubleValue(),
				Currency.getInstance(CURRENCY).getCurrencyCode());

		AccountEntity destinationAccount = new AccountEntity(ACCOUNT_ID_2, ACCOUNT_NAME_2,
				new BigDecimal(DESTINATION_AMOUNT).setScale(ROUND_SCALE_VALUE, RoundingMode.UNNECESSARY).doubleValue(),
				Currency.getInstance(CURRENCY).getCurrencyCode());

		final BigDecimal newSourceAmount = new BigDecimal(sourceAccount.getBalance())
				.setScale(2, BigDecimal.ROUND_UNNECESSARY).subtract(new BigDecimal(transferModel.getAmount())
						.setScale(ROUND_SCALE_VALUE, BigDecimal.ROUND_UNNECESSARY));

		final BigDecimal newDestinationAmount = new BigDecimal(destinationAccount.getBalance())
				.setScale(ROUND_SCALE_VALUE, BigDecimal.ROUND_UNNECESSARY).add(new BigDecimal(transferModel.getAmount())
						.setScale(ROUND_SCALE_VALUE, BigDecimal.ROUND_UNNECESSARY));

		when(moneyTransferModelMoneyTransferControllerModelMapper.map(Mockito.isA(MoneyTransferControllerModel.class)))
				.thenReturn(transferModel);

		when(accountRepository.getAccountForUpdate(SOURCE_ACCOUNT_ID)).thenReturn(sourceAccount);
		when(accountRepository.getAccountForUpdate(DESTINATIO_ACCOUNT_ID)).thenReturn(destinationAccount);

		when(accountRepository.updateBalance(transferModel.getSourceAccountId(), newSourceAmount.doubleValue()))
				.thenReturn(SOURCE_ACCOUNT_ID);
		when(accountRepository.updateBalance(transferModel.getDestinationAccountId(),
				newDestinationAmount.doubleValue())).thenReturn(DESTINATIO_ACCOUNT_ID);

		doNothing().when(transferService).transfer(transferModel);

		final Response post = resources.client().target(MONEY_TRANSFER_URL).request().post(Entity.json(transfer));
		assertThat(post.getStatus()).isEqualTo(204);

	}
}
