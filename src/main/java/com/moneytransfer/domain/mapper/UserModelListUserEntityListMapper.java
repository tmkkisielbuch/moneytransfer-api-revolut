package com.moneytransfer.domain.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.moneytransfer.domain.model.UserModel;
import com.moneytransfer.domain.utils.IMapper;
import com.moneytransfer.infrastructure.entity.UserEntity;

public class UserModelListUserEntityListMapper implements IMapper<List<UserModel>, List<UserEntity>> {

	@Override
	public List<UserModel> map(List<UserEntity> model, Object... params) {
		List<UserModel> users = model.stream().map(temp -> {
			UserModel user = new UserModel();
			user.setId(temp.getId());
			user.setEmail(temp.getEmail());
			user.setUsername(temp.getUsername());
			return user;
		}).collect(Collectors.toList());

		return users;
	}

}
