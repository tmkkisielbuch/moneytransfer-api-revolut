package com.moneytransfer.domain.mapper;

import com.moneytransfer.domain.model.CurrencyAmountModel;
import com.moneytransfer.domain.utils.IMapper;
import com.moneytransfer.infrastructure.entity.AccountEntity;

public class CurrencyAmountModelAccountEntityMapper implements IMapper<CurrencyAmountModel, AccountEntity> {

	@Override
	public CurrencyAmountModel map(AccountEntity model, Object... params) {
		CurrencyAmountModel currencyAmountModel = new CurrencyAmountModel();
		if (model != null) {
			currencyAmountModel.setAmount(model.getBalance());
			currencyAmountModel.setCurrency(model.getCurrency());
		}
		return currencyAmountModel;
	}

}
