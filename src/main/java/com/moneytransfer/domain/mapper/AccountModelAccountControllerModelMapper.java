package com.moneytransfer.domain.mapper;

import com.moneytransfer.controller.model.AccountControllerModel;
import com.moneytransfer.domain.model.AccountModel;
import com.moneytransfer.domain.utils.IMapper;

public class AccountModelAccountControllerModelMapper implements IMapper<AccountModel, AccountControllerModel> {

	@Override
	public AccountModel map(AccountControllerModel model, Object... params) {
		AccountModel account = new AccountModel();
		if (model != null) {
			account.setBalanceAmount(model.getBalanceAmount());
			account.setCurrency(model.getCurrency());
			account.setUsername(model.getUsername());
		}
		return account;
	}

}
