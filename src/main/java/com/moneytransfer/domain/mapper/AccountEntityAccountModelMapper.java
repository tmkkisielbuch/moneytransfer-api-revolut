package com.moneytransfer.domain.mapper;

import com.moneytransfer.domain.model.AccountModel;
import com.moneytransfer.domain.utils.IMapper;
import com.moneytransfer.infrastructure.entity.AccountEntity;

public class AccountEntityAccountModelMapper implements IMapper<AccountEntity, AccountModel> {

	@Override
	public AccountEntity map(AccountModel model, Object... params) {
		AccountEntity entity = new AccountEntity();
		if (model != null) {
			entity.setBalance(model.getBalanceAmount());
			entity.setCurrency(model.getCurrency());
			entity.setUsername(model.getUsername());
		}
		return entity;
	}

}
