package com.moneytransfer.domain.mapper;

import com.moneytransfer.domain.model.AccountModel;
import com.moneytransfer.domain.utils.IMapper;
import com.moneytransfer.infrastructure.entity.AccountEntity;

public class AccountModelAccountEntityMapper implements IMapper<AccountModel, AccountEntity> {

	@Override
	public AccountModel map(AccountEntity model, Object... params) {
		AccountModel account = new AccountModel();
		if (model != null) {
			account.setBalanceAmount(model.getBalance());
			account.setCurrency(model.getCurrency());
			account.setUsername(model.getUsername());
			account.setId(model.getId());
		}
		return account;
	}

}
