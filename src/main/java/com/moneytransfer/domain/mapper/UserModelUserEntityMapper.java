package com.moneytransfer.domain.mapper;

import com.moneytransfer.domain.model.UserModel;
import com.moneytransfer.domain.utils.IMapper;
import com.moneytransfer.infrastructure.entity.UserEntity;

public class UserModelUserEntityMapper implements IMapper<UserModel, UserEntity> {

	@Override
	public UserModel map(UserEntity model, Object... params) {
		UserModel user = new UserModel();
		if (model != null) {
			user.setId(model.getId());
			user.setEmail(model.getEmail());
			user.setUsername(model.getUsername());
		}
		return user;
	}

}
