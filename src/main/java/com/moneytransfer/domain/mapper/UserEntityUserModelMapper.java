package com.moneytransfer.domain.mapper;

import com.moneytransfer.domain.model.UserModel;
import com.moneytransfer.domain.utils.IMapper;
import com.moneytransfer.infrastructure.entity.UserEntity;

public class UserEntityUserModelMapper implements IMapper<UserEntity, UserModel> {

	@Override
	public UserEntity map(UserModel model, Object... params) {
		UserEntity user = new UserEntity();
		if (model != null) {
			user.setEmail(model.getEmail());
			user.setUsername(model.getUsername());
		}
		return user;
	}

}
