package com.moneytransfer.domain.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.moneytransfer.domain.model.AccountModel;
import com.moneytransfer.domain.utils.IMapper;
import com.moneytransfer.infrastructure.entity.AccountEntity;

public class AccountModelListAccountEntityListMapper implements IMapper<List<AccountModel>, List<AccountEntity>> {

	@Override
	public List<AccountModel> map(List<AccountEntity> model, Object... params) {

		List<AccountModel> accounts = model.stream().map(temp -> {
			AccountModel account = new AccountModel(temp.getId(), temp.getUsername(), temp.getBalance(),
					temp.getCurrency());
			return account;
		}).collect(Collectors.toList());
		return accounts;
	}

}
