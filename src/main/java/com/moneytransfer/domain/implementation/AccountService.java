package com.moneytransfer.domain.implementation;

import java.math.BigDecimal;
import java.util.List;

import com.moneytransfer.domain.enumerator.Error;
import com.moneytransfer.domain.exception.ApplicationException;
import com.moneytransfer.domain.mapper.AccountEntityAccountModelMapper;
import com.moneytransfer.domain.mapper.AccountModelAccountEntityMapper;
import com.moneytransfer.domain.mapper.AccountModelListAccountEntityListMapper;
import com.moneytransfer.domain.mapper.CurrencyAmountModelAccountEntityMapper;
import com.moneytransfer.domain.model.AccountModel;
import com.moneytransfer.domain.model.CurrencyAmountModel;
import com.moneytransfer.domain.service.IAccountService;
import com.moneytransfer.infrastructure.entity.AccountEntity;
import com.moneytransfer.infrastructure.repository.IAccountRepository;

public class AccountService implements IAccountService {

	IAccountRepository repository;
	AccountModelAccountEntityMapper accountModelAccountEntityMapper;
	AccountEntityAccountModelMapper accountEntityAccountModelMapper;
	AccountModelListAccountEntityListMapper accountModelListAccountEntityListMapper;
	CurrencyAmountModelAccountEntityMapper currencyAmountModelAccountEntityMapper;

	public AccountService() {
		super();
	}

	public AccountService(IAccountRepository repository) {
		super();
		this.repository = repository;
		accountModelAccountEntityMapper = new AccountModelAccountEntityMapper();
		accountEntityAccountModelMapper = new AccountEntityAccountModelMapper();
		accountModelListAccountEntityListMapper = new AccountModelListAccountEntityListMapper();
		currencyAmountModelAccountEntityMapper = new CurrencyAmountModelAccountEntityMapper();
	}

	@Override
	public AccountModel saveAccount(AccountModel model) {

		if (repository.getUserAccount(model.getUsername(), model.getCurrency()) != null) {
			throw new ApplicationException(Error.AccountExist.getCode(), Error.AccountExist.getMessage());
		}
		AccountEntity accountEntity = accountEntityAccountModelMapper.map(model);
		int id = repository.save(accountEntity);
		AccountEntity accountEntityResponse = repository.findAccountById(id);

		return accountModelAccountEntityMapper.map(accountEntityResponse);
	}

	@Override
	public AccountModel getAccount(int id) {
		AccountEntity accountEntity = repository.findAccountById(id);
		return accountModelAccountEntityMapper.map(accountEntity);
	}

	@Override
	public List<AccountModel> getAllAccounts() {
		List<AccountEntity> accountEntities = repository.getAllAccounts();
		return accountModelListAccountEntityListMapper.map(accountEntities);
	}

	@Override
	public CurrencyAmountModel getBalanceAmount(int id) {
		AccountEntity accountEntity = repository.findAccountById(id);
		if (accountEntity == null) {
			throw new ApplicationException(Error.AccountNotExist.getCode(), Error.AccountNotExist.getMessage());
		}
		return currencyAmountModelAccountEntityMapper.map(accountEntity);
	}

	@Override
	public void deleteUser(String username) {
		repository.delete(username);
	}

	@Override
	public AccountModel updateAccount(int id, AccountModel model) {

		AccountEntity accountEntity = repository.findAccountById(id);
		if (accountEntity == null) {
			throw new ApplicationException(Error.AccountNotExist.getCode(), Error.AccountNotExist.getMessage());
		}
		AccountEntity accountEntityUpdateModel = accountEntityAccountModelMapper.map(model);
		repository.updateAccount(accountEntityUpdateModel, id);
		AccountEntity accountEntityResponse = repository.findAccountById(id);
		return accountModelAccountEntityMapper.map(accountEntityResponse);
	}

	@Override
	public AccountModel deposit(String username, CurrencyAmountModel currencyAmount) {
		return null;
	}

	@Override
	public AccountModel withdraw(String username, CurrencyAmountModel currencyAmount) {

		AccountEntity accountEntity = repository.getUserAccountForUpdate(username, currencyAmount.getCurrency());

		if (accountEntity == null) {
			throw new ApplicationException(Error.AccountNotExist.getCode(), Error.AccountNotExist.getMessage());
		}

		if (currencyAmount.getAmount() > accountEntity.getBalance()) {
			throw new ApplicationException(Error.Withdrawal.getCode(), Error.Withdrawal.getMessage());
		}

		double newBalance = new BigDecimal(accountEntity.getBalance())
				.subtract(new BigDecimal(currencyAmount.getAmount())).doubleValue();
		int accountId = repository.updateBalance(accountEntity.getId(), newBalance);
		AccountEntity accountEntityResponse = repository.findAccountById(accountId);
		return accountModelAccountEntityMapper.map(accountEntityResponse);
	}
}
