package com.moneytransfer.domain.implementation;

import java.math.BigDecimal;

import com.moneytransfer.domain.enumerator.Error;
import com.moneytransfer.domain.exception.ApplicationException;
import com.moneytransfer.domain.model.MoneyTransferModel;
import com.moneytransfer.domain.service.IMoneyTransactionService;
import com.moneytransfer.infrastructure.entity.AccountEntity;
import com.moneytransfer.infrastructure.repository.IAccountRepository;

public class MoneyTransactionService implements IMoneyTransactionService {

	IAccountRepository accountRepository;

	public MoneyTransactionService() {
		super();
	}

	public MoneyTransactionService(IAccountRepository accountRepository) {
		super();
		this.accountRepository = accountRepository;
	}

	@Override
	public void transfer(MoneyTransferModel transferModel) {
		AccountEntity sourceAccount = accountRepository.getAccountForUpdate(transferModel.getSourceAccountId());
		AccountEntity destinationAccount = accountRepository
				.getAccountForUpdate(transferModel.getDestinationAccountId());

		validateAccounts(sourceAccount, destinationAccount, transferModel);

		final BigDecimal newSourceAmount = new BigDecimal(sourceAccount.getBalance())
				.setScale(2, BigDecimal.ROUND_UNNECESSARY)
				.subtract(new BigDecimal(transferModel.getAmount()).setScale(2, BigDecimal.ROUND_UNNECESSARY));

		final BigDecimal newDestinationAmount = new BigDecimal(destinationAccount.getBalance())
				.setScale(2, BigDecimal.ROUND_UNNECESSARY)
				.add(new BigDecimal(transferModel.getAmount()).setScale(2, BigDecimal.ROUND_UNNECESSARY));

		accountRepository.updateBalance(transferModel.getSourceAccountId(), newSourceAmount.doubleValue());
		accountRepository.updateBalance(transferModel.getDestinationAccountId(), newDestinationAmount.doubleValue());
	}

	public void validateAccounts(AccountEntity sourceAccount, AccountEntity destinationAccount,
			MoneyTransferModel transferModel) {

		if (sourceAccount == null) {
			throw new ApplicationException(Error.SourceAccountExist.getCode(), Error.SourceAccountExist.getMessage());
		}

		if (destinationAccount == null) {
			throw new ApplicationException(Error.DestinationAccountExist.getCode(),
					Error.DestinationAccountExist.getMessage());
		}

		if (!transferModel.getCurrency().equals(sourceAccount.getCurrency())) {
			throw new ApplicationException(Error.CurrenciesDoNotMatch.getCode(),
					Error.CurrenciesDoNotMatch.getMessage());
		}

		if (!sourceAccount.getCurrency().equals(destinationAccount.getCurrency())) {
			throw new ApplicationException(Error.AccountCurrenciesDoNotMatch.getCode(),
					Error.AccountCurrenciesDoNotMatch.getMessage());

		}

		if (transferModel.getAmount() > sourceAccount.getBalance()) {
			throw new ApplicationException(Error.NotEnoughBalance.getCode(), Error.NotEnoughBalance.getMessage());

		}
	}
}
