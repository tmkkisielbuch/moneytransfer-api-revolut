package com.moneytransfer.domain.implementation;

import java.util.List;

import com.moneytransfer.domain.enumerator.Error;
import com.moneytransfer.domain.exception.ApplicationException;
import com.moneytransfer.domain.mapper.AccountModelAccountEntityMapper;
import com.moneytransfer.domain.mapper.UserEntityUserModelMapper;
import com.moneytransfer.domain.mapper.UserModelListUserEntityListMapper;
import com.moneytransfer.domain.mapper.UserModelUserEntityMapper;
import com.moneytransfer.domain.model.AccountModel;
import com.moneytransfer.domain.model.UserModel;
import com.moneytransfer.domain.service.IUserService;
import com.moneytransfer.infrastructure.entity.AccountEntity;
import com.moneytransfer.infrastructure.entity.UserEntity;
import com.moneytransfer.infrastructure.repository.IAccountRepository;
import com.moneytransfer.infrastructure.repository.IUserRepository;

public class UserService implements IUserService {

	IUserRepository userRepository;
	IAccountRepository accountRepository;
	UserModelUserEntityMapper userModelUserEntityMapper;
	UserEntityUserModelMapper userEntityUserModelMapper;
	UserModelListUserEntityListMapper userModelListUserEntityListMapper;
	AccountModelAccountEntityMapper accountModelAccountEntityMapper;

	public UserService() {
		super();
	}

	public UserService(IUserRepository userRepository, IAccountRepository accountRepository) {
		super();
		this.userRepository = userRepository;
		this.accountRepository = accountRepository;
		userModelUserEntityMapper = new UserModelUserEntityMapper();
		userEntityUserModelMapper = new UserEntityUserModelMapper();
		userModelListUserEntityListMapper = new UserModelListUserEntityListMapper();
		accountModelAccountEntityMapper = new AccountModelAccountEntityMapper();
	}

	@Override
	public UserModel saveUser(UserModel user) {
		UserEntity userEntity = userEntityUserModelMapper.map(user);
		if (userRepository.getUserByUsername(user.getUsername()) != null) {
			throw new ApplicationException(Error.UserExist.getCode(), Error.UserExist.getMessage());
		}
		int id = userRepository.createUser(userEntity);
		UserEntity userEntityResponse = userRepository.findUserById(id);
		return userModelUserEntityMapper.map(userEntityResponse);
	}

	@Override
	public List<UserModel> getAllUsers() {
		List<UserEntity> userEntities = userRepository.getAllUsers();
		return userModelListUserEntityListMapper.map(userEntities);
	}

	@Override
	public UserModel getUser(String username) {
		UserEntity userEntity = userRepository.getUserByUsername(username);
		return userModelUserEntityMapper.map(userEntity);
	}

	@Override
	public UserModel updateUser(int id, UserModel user) {

		UserEntity userEntity = userRepository.findUserById(id);
		if (userEntity == null) {
			throw new ApplicationException(Error.UserNotExist.getCode(), Error.UserNotExist.getMessage());
		}
		UserEntity userEntityUpdateModel = userEntityUserModelMapper.map(user);
		userRepository.updateUser(userEntityUpdateModel, id);

		UserEntity userEntityResponse = userRepository.findUserById(id);
		return userModelUserEntityMapper.map(userEntityResponse);
	}

	@Override
	public void deleteUser(int id) {
		userRepository.deleteUser(id);

	}

	@Override
	public AccountModel account(String username, IAccountRepository accountRepository) {

		UserEntity userEntity = userRepository.getUserByUsername(username);

		if (userEntity == null) {
			throw new ApplicationException(Error.UserNotExist.getCode(), Error.UserNotExist.getMessage());
		}

		AccountEntity accountEntity = accountRepository.getUserAccount(username);

		if (accountEntity == null) {
			throw new ApplicationException(Error.AccountNotExist.getCode(), Error.AccountNotExist.getMessage());
		}

		return accountModelAccountEntityMapper.map(accountEntity);
	}
}
