package com.moneytransfer.domain.service;

import com.moneytransfer.domain.model.MoneyTransferModel;

public interface IMoneyTransactionService {
    void transfer(MoneyTransferModel transfer);
}
