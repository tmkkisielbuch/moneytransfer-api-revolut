package com.moneytransfer.domain.service;

import com.moneytransfer.domain.model.AccountModel;
import com.moneytransfer.domain.model.UserModel;
import com.moneytransfer.infrastructure.repository.IAccountRepository;

import java.util.List;

public interface IUserService {
	List<UserModel> getAllUsers();

	UserModel getUser(String username);

	AccountModel account(String username, IAccountRepository accountRepository);

	UserModel saveUser(UserModel user);

	UserModel updateUser(int id, UserModel user);

	void deleteUser(int id);
}
