package com.moneytransfer.domain.service;

import java.util.List;

import com.moneytransfer.domain.model.AccountModel;
import com.moneytransfer.domain.model.CurrencyAmountModel;

public interface IAccountService {
	AccountModel getAccount(int id);

	List<AccountModel> getAllAccounts();

	CurrencyAmountModel getBalanceAmount(int id);

	AccountModel saveAccount(AccountModel accountModel);

	AccountModel updateAccount(int id, AccountModel accountModel);

	void deleteUser(String username);

	AccountModel deposit(String username, CurrencyAmountModel currencyAmount);

	AccountModel withdraw(String username, CurrencyAmountModel currencyAmount);
}
