package com.moneytransfer.domain.model;

public class AccountModel {
	private int id;
	private String username;
	private Double balanceAmount;
	private String currency;

	public AccountModel() {
		super();
	}

	public AccountModel(String username, Double balanceAmount, String currency) {
		super();
		this.username = username;
		this.balanceAmount = balanceAmount;
		this.currency = currency;
	}

	public AccountModel(int id, String username, Double balanceAmount, String currency) {
		super();
		this.id = id;
		this.username = username;
		this.balanceAmount = balanceAmount;
		this.currency = currency;
	}

	public Double getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(Double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
