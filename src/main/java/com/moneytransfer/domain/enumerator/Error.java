package com.moneytransfer.domain.enumerator;

public enum Error {
	AccountExist("accountExist", "This account is exist with given currency"),
	AccountNotExist("accountNotExist", "This account is not  exist with given id"),
	Withdrawal("withdrawal", "withdrawal amount cannot be greater than balance amount "),
	UserExist("userExist", "This user is exist"), UserNotExist("userNotExist", "This user is not  exist with given id"),
	SourceAccountExist("sourceAccountExist", "Source Account is not found"),
	DestinationAccountExist("DestinationAccountExist", "Destination account is not found"),
	CurrenciesDoNotMatch("currenciesDoNotMatch", "Currencies do not Mmatch"),
	AccountCurrenciesDoNotMatch("accountCurrenciesDoNotMatch", " Account currencies do not Mmatch"),
	NotEnoughBalance("notEnoughBalance", "Balance is not enough");



	private final String code;
	private final String message;

	Error(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
}
