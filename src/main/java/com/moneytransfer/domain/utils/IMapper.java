package com.moneytransfer.domain.utils;

public interface IMapper<Target, Source> {

    Target map(Source model, Object... params);
}

