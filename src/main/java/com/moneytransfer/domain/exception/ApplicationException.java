package com.moneytransfer.domain.exception;

import javax.ws.rs.WebApplicationException;

public class ApplicationException extends WebApplicationException {
	private static final long serialVersionUID = -8894710597533900652L;

	private final String code;
	private final String description;

	public ApplicationException(String code, String description) {
		super(message(code, description));

		this.code = code;
		this.description = description;
	}

	public ApplicationException(String code, String description, Throwable cause) {
		super(message(code, description), cause);

		this.code = code;
		this.description = description;
	}

	public String getCode() {

		return code;
	}

	public String getDescription() {

		return description;
	}

	private static String message(String code, String description) {

		return description;
	}
}
