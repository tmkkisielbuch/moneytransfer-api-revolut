package com.moneytransfer.domain.exception;

import com.moneytransfer.domain.enumerator.Exception;
import org.glassfish.jersey.internal.Errors;

public class ValidationException extends ApplicationException {
    private static final long serialVersionUID = 1L;

    private final Errors errors;

    public ValidationException(Errors errors, String description) {
        super(Exception.VALIDATION.toString(), description);

        this.errors = errors;
    }

    public ValidationException(Errors errors) {
        super(Exception.VALIDATION.toString(), message(errors));

        this.errors = errors;
    }

    private static String message(Errors errors) {
        return null;
    }

    public Errors getErrors() {
        return errors;
    }

//    private static String message(Errors errors) {
//        String fields = getFields(errors);
//        return "Validation errors for fields: " + fields;
//    }
//
//    private static String getFields(Errors errors) {
//        List<String> fieldNames = new ArrayList<>();
//        for (FieldError e : errors.getFieldErrors())
//            fieldNames.add(e.getField());
//
//        return StringUtils.join(fieldNames, ",");
//    }
}
