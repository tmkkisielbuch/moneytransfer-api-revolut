package com.moneytransfer;

import org.skife.jdbi.v2.DBI;

import com.moneytransfer.controller.AccountController;
import com.moneytransfer.controller.MoneyTransactionController;
import com.moneytransfer.controller.UserController;
import com.moneytransfer.controller.mapper.AccountControllerModelAccountModelMapper;
import com.moneytransfer.controller.mapper.AccountControllerModelListAccountModelListMapper;
import com.moneytransfer.controller.mapper.CurrencyAmountControllerModelCurrencyAmountModelMapper;
import com.moneytransfer.controller.mapper.MoneyTransferModelMoneyTransferControllerModelMapper;
import com.moneytransfer.controller.mapper.UserControllerModelListUserModelListMapper;
import com.moneytransfer.controller.mapper.UserControllerModelUserModelMapper;
import com.moneytransfer.controller.mapper.UserModelUserControllerModelMapper;
import com.moneytransfer.domain.config.DbConfiguration;
import com.moneytransfer.domain.implementation.AccountService;
import com.moneytransfer.domain.implementation.MoneyTransactionService;
import com.moneytransfer.domain.implementation.UserService;
import com.moneytransfer.domain.mapper.AccountModelAccountControllerModelMapper;
import com.moneytransfer.infrastructure.repository.IAccountRepository;
import com.moneytransfer.infrastructure.repository.IUserRepository;

import io.dropwizard.Application;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Environment;

public class App extends Application<DbConfiguration> {
	public static void main(String[] args) throws Exception {
		new App().run(args);
	}

	@Override
	public void run(DbConfiguration moneyMateDbConfiguration, Environment environment) throws Exception {

		final DBIFactory factory = new DBIFactory();
		final DBI jdbi = factory.build(environment, moneyMateDbConfiguration.getDataSourceFactory(), "h2");
		final IUserRepository userRepository = jdbi.onDemand(IUserRepository.class);
		final IAccountRepository accountRepository = jdbi.onDemand(IAccountRepository.class);
		
		userRepository.createTable();
		accountRepository.createTable();

		UserModelUserControllerModelMapper userModelUserControllerModelMapper = new UserModelUserControllerModelMapper();
		UserControllerModelUserModelMapper userControllerModelUserModelMapper = new UserControllerModelUserModelMapper();
		UserControllerModelListUserModelListMapper userControllerModelListUserModelListMapper = new UserControllerModelListUserModelListMapper();
		AccountControllerModelAccountModelMapper userAccountControllerModelAccountModelMapper = new AccountControllerModelAccountModelMapper();
		UserService userService = new UserService();

		UserController userController = new UserController(userService, userRepository,
				userModelUserControllerModelMapper, userControllerModelUserModelMapper,
				userControllerModelListUserModelListMapper, accountRepository,
				userAccountControllerModelAccountModelMapper);

		AccountService accountService = new AccountService();

		AccountModelAccountControllerModelMapper accountModelAccountControllerModelMapper = new AccountModelAccountControllerModelMapper();
		AccountControllerModelAccountModelMapper accountControllerModelAccountModelMapper = new AccountControllerModelAccountModelMapper();
		AccountControllerModelListAccountModelListMapper accountControllerModelListAccountModelListMapper = new AccountControllerModelListAccountModelListMapper();
		CurrencyAmountControllerModelCurrencyAmountModelMapper currencyAmountControllerModelCurrencyAmountModelMapper = new CurrencyAmountControllerModelCurrencyAmountModelMapper();

		AccountController accountController = new AccountController(accountRepository, accountService,
				accountModelAccountControllerModelMapper, accountControllerModelAccountModelMapper,
				accountControllerModelListAccountModelListMapper,
				currencyAmountControllerModelCurrencyAmountModelMapper);

		MoneyTransactionService transferService = new MoneyTransactionService();
		MoneyTransferModelMoneyTransferControllerModelMapper moneyTransferModelMoneyTransferControllerModelMapper = new MoneyTransferModelMoneyTransferControllerModelMapper();
		MoneyTransactionController transferController = new MoneyTransactionController(accountRepository,
				transferService, moneyTransferModelMoneyTransferControllerModelMapper);

		environment.jersey().register(userController);
		environment.jersey().register(accountController);
		environment.jersey().register(transferController);

	}
}
