package com.moneytransfer.infrastructure.mapper;

import com.moneytransfer.infrastructure.entity.AccountEntity;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountEntityMapper implements ResultSetMapper<AccountEntity> {
	@Override
	public AccountEntity map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
		return new AccountEntity(resultSet.getInt("id"), resultSet.getString("username"),
				resultSet.getBigDecimal("balance").doubleValue(), resultSet.getString("currency"));
	}
}
