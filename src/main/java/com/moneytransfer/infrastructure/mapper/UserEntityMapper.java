package com.moneytransfer.infrastructure.mapper;

import com.moneytransfer.infrastructure.entity.UserEntity;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
public class UserEntityMapper implements ResultSetMapper<UserEntity> {

    @Override
    public UserEntity map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new UserEntity(resultSet.getInt("id"), resultSet.getString("username"), resultSet.getString("email"));
    }
}
