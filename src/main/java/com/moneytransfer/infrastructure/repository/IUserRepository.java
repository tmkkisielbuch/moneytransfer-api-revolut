package com.moneytransfer.infrastructure.repository;

import com.moneytransfer.infrastructure.entity.UserEntity;
import com.moneytransfer.infrastructure.mapper.UserEntityMapper;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.*;

import java.util.List;

@RegisterMapper(UserEntityMapper.class)
public interface IUserRepository {
	@SqlUpdate("CREATE TABLE user (id INT PRIMARY KEY AUTO_INCREMENT NOT NULL," + " username varchar(25), "
			+ "email varchar(25))")
	void createTable();

	@SqlQuery("SELECT * FROM user where id = :id")
	UserEntity findUserById(@Bind("id") int id);

	@SqlQuery("SELECT * FROM user WHERE username = :username")
	UserEntity getUserByUsername(@Bind("username") String username);

	@SqlQuery("SELECT * from user")
	List<UserEntity> getAllUsers();

	@SqlUpdate("INSERT INTO user (username, email) VALUES (:u.username, :u.email)")
	@GetGeneratedKeys
	@Transaction
	int createUser(@BindBean("u") UserEntity user);

	@SqlUpdate("UPDATE user SET username = :u.username, email = :u.email WHERE id = :id")
	int updateUser(@BindBean("u") UserEntity user, @Bind("id") int id);

	@SqlUpdate("DELETE FROM user WHERE id = :id")
	void deleteUser(@Bind("id") int id);
}
