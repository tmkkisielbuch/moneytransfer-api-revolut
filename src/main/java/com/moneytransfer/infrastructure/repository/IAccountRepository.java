package com.moneytransfer.infrastructure.repository;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.Transaction;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import com.moneytransfer.infrastructure.entity.AccountEntity;
import com.moneytransfer.infrastructure.mapper.AccountEntityMapper;

@RegisterMapper(AccountEntityMapper.class)
public interface IAccountRepository {

	@SqlUpdate("CREATE TABLE account (id INT PRIMARY KEY AUTO_INCREMENT NOT NULL," + " username varchar(50),"
			+ " currency varchar(3)," + " balance decimal(20,2))")
	void createTable();

	@SqlQuery("SELECT * FROM account WHERE id = :id")
	AccountEntity findAccountById(@Bind("id") int id);

	@SqlQuery("SELECT * FROM account")
	List<AccountEntity> getAllAccounts();

	@SqlQuery("SELECT * FROM account WHERE username = :username")
	AccountEntity getUserAccount(@Bind("username") String username);

	@SqlQuery("SELECT * FROM account WHERE username = :username AND currency = :currency")
	AccountEntity getUserAccount(@Bind("username") String username, @Bind("currency") String currency);

	@SqlQuery("SELECT * FROM account WHERE username = :username AND currency = :currency FOR UPDATE")
	AccountEntity getUserAccountForUpdate(@Bind("username") String username, @Bind("currency") String currency);

	@SqlQuery("SELECT * FROM account WHERE id = :id FOR UPDATE")
	AccountEntity getAccountForUpdate(@Bind("id") int id);

	@SqlUpdate("INSERT INTO account (username, currency, balance) " + "values (:a.username, :a.currency, :a.balance)")
	@GetGeneratedKeys
	@Transaction
	int save(@BindBean("a") AccountEntity account);

	@SqlUpdate("UPDATE account SET username = :a.username," + " currency = :a.currency,"
			+ " balance = :a.balance WHERE id = :id")
	int updateAccount(@BindBean("a") AccountEntity account, @Bind("id") int id);

	@SqlUpdate("UPDATE account SET balance = :balance WHERE id = :id")
	int updateBalance(@Bind("id") int id, @Bind("balance") double balance);

	@SqlUpdate("DELETE FROM account WHERE username = :username")
	void delete(@Bind("username") String username);
}
