package com.moneytransfer.infrastructure.entity;

public class UserEntity {

    private int id;
    private String username;
    private String email;

    public UserEntity() {}

    public UserEntity(String username, String email) {
        this.username = username;
        this.email = email;
    }

    public UserEntity(int id, String username, String email) {
        this.id = id;
        this.username = username;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
