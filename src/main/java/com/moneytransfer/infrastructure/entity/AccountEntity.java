package com.moneytransfer.infrastructure.entity;

public class AccountEntity {

	private int id;
	private String username;
	private Double balance;
	private String currency;

	public AccountEntity() {
	}

	public AccountEntity(String username, Double balance, String currency) {
		this.username = username;
		this.balance = balance;
		this.currency = currency;
	}

	public AccountEntity(int id, String username, Double balance, String currency) {
		this.id = id;
		this.username = username;
		this.balance = balance;
		this.currency = currency;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
}