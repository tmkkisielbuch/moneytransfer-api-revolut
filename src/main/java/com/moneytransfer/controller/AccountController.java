package com.moneytransfer.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.validator.constraints.NotBlank;
import org.skife.jdbi.v2.sqlobject.Transaction;

import com.codahale.metrics.annotation.Timed;
import com.moneytransfer.controller.mapper.AccountControllerModelAccountModelMapper;
import com.moneytransfer.controller.mapper.AccountControllerModelListAccountModelListMapper;
import com.moneytransfer.controller.mapper.CurrencyAmountControllerModelCurrencyAmountModelMapper;
import com.moneytransfer.controller.model.AccountControllerModel;
import com.moneytransfer.controller.model.CurrencyAmountControllerModel;
import com.moneytransfer.domain.implementation.AccountService;
import com.moneytransfer.domain.mapper.AccountModelAccountControllerModelMapper;
import com.moneytransfer.domain.model.AccountModel;
import com.moneytransfer.domain.model.CurrencyAmountModel;
import com.moneytransfer.infrastructure.repository.IAccountRepository;

@Path("/api/accounts")
@Produces(MediaType.APPLICATION_JSON)
public class AccountController {

	AccountService accountService;
	IAccountRepository repository;
	AccountModelAccountControllerModelMapper accountModelAccountControllerModelMapper;
	AccountControllerModelAccountModelMapper accountControllerModelAccountModelMapper;
	AccountControllerModelListAccountModelListMapper accountControllerModelListAccountModelListMapper;
	CurrencyAmountControllerModelCurrencyAmountModelMapper currencyAmountControllerModelCurrencyAmountModelMapper;

	public AccountController() {
	}

	public AccountController(IAccountRepository repository, AccountService accountService,
			AccountModelAccountControllerModelMapper accountModelAccountControllerModelMapper,
			AccountControllerModelAccountModelMapper accountControllerModelAccountModelMapper,
			AccountControllerModelListAccountModelListMapper accountControllerModelListAccountModelListMapper,
			CurrencyAmountControllerModelCurrencyAmountModelMapper currencyAmountControllerModelCurrencyAmountModelMapper) {
		this.repository = repository;
		accountService = new AccountService(repository);
		this.accountService = accountService;
		this.accountModelAccountControllerModelMapper = accountModelAccountControllerModelMapper;
		this.accountControllerModelAccountModelMapper = accountControllerModelAccountModelMapper;
		this.accountControllerModelListAccountModelListMapper = accountControllerModelListAccountModelListMapper;
		this.currencyAmountControllerModelCurrencyAmountModelMapper = currencyAmountControllerModelCurrencyAmountModelMapper;

	}

	@POST
	@Timed
	public AccountControllerModel createAccount(@Valid AccountControllerModel account) {
		AccountModel accountModel = accountModelAccountControllerModelMapper.map(account);
		AccountModel accountResponseModel = accountService.saveAccount(accountModel);
		return accountControllerModelAccountModelMapper.map(accountResponseModel);
	}

	@GET
	@Timed
	@Path("/{id}")
	public AccountControllerModel getAccount(@PathParam("id") int id) {
		AccountModel accountModel = accountService.getAccount(id);
		return accountControllerModelAccountModelMapper.map(accountModel);
	}

	@GET
	@Timed
	public List<AccountControllerModel> getAllAccounts() {
		List<AccountModel> accountModelList = accountService.getAllAccounts();
		return accountControllerModelListAccountModelListMapper.map(accountModelList);
	}

	@DELETE
	@Timed
	@Path("/{username}")
	public void deleteAccount(@PathParam("username") @NotBlank String username) {
		accountService.deleteUser(username);
	}

	@GET
	@Timed
	@Path("/{id}/balanceAmount")
	public CurrencyAmountControllerModel getBalance(@PathParam("id") @NotNull Integer id) {
		CurrencyAmountModel currencyAmountModel = accountService.getBalanceAmount(id);
		return currencyAmountControllerModelCurrencyAmountModelMapper.map(currencyAmountModel);
	}

	@PUT
	@Timed
	@Path("/{id}")
	@Transaction
	public AccountControllerModel updateAccount(@PathParam("id") @NotNull Integer id, @Valid AccountControllerModel account) {
		AccountModel accountModel = accountModelAccountControllerModelMapper.map(account);
		AccountModel accountResponseModel = accountService.updateAccount(id,accountModel);
		return accountControllerModelAccountModelMapper.map(accountResponseModel);
	}

	@PUT
	@Timed
	@Path("/{username}/withdraw")
	public AccountControllerModel withdraw(@PathParam("username") String username,
			@Valid CurrencyAmountModel currencyAmount) {
		accountService.withdraw(username, currencyAmount);
		return null;
	}

}
