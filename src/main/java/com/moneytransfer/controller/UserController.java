package com.moneytransfer.controller;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.validator.constraints.NotEmpty;

import com.codahale.metrics.annotation.Timed;
import com.moneytransfer.controller.mapper.AccountControllerModelAccountModelMapper;
import com.moneytransfer.controller.mapper.UserControllerModelListUserModelListMapper;
import com.moneytransfer.controller.mapper.UserControllerModelUserModelMapper;
import com.moneytransfer.controller.mapper.UserModelUserControllerModelMapper;
import com.moneytransfer.controller.model.AccountControllerModel;
import com.moneytransfer.controller.model.UserControllerModel;
import com.moneytransfer.domain.implementation.UserService;
import com.moneytransfer.domain.model.AccountModel;
import com.moneytransfer.domain.model.UserModel;
import com.moneytransfer.infrastructure.repository.IAccountRepository;
import com.moneytransfer.infrastructure.repository.IUserRepository;

@Path("/api/users")
@Produces(MediaType.APPLICATION_JSON)
public class UserController {

	UserService userService;
	IUserRepository userRepository;
	IAccountRepository accountRepository;
	UserModelUserControllerModelMapper userModelUserControllerModelMapper;
	UserControllerModelUserModelMapper userControllerModelUserModelMapper;
	UserControllerModelListUserModelListMapper userControllerModelListUserModelListMapper;
	AccountControllerModelAccountModelMapper accountControllerModelAccountModelMapper;

	public UserController() {
		super();
	}

	public UserController(UserService userService, IUserRepository userRepository,
			UserModelUserControllerModelMapper userModelUserControllerModelMapper,
			UserControllerModelUserModelMapper userControllerModelUserModelMapper,
			UserControllerModelListUserModelListMapper userControllerModelListUserModelListMapper,
			IAccountRepository accountRepository,
			AccountControllerModelAccountModelMapper accountControllerModelAccountModelMapper) {
		super();
		this.userRepository = userRepository;
		this.accountRepository = accountRepository;
		userService = new UserService(userRepository, accountRepository);
		this.userService = userService;
		this.userControllerModelUserModelMapper = userControllerModelUserModelMapper;
		this.userModelUserControllerModelMapper = userModelUserControllerModelMapper;
		this.userControllerModelListUserModelListMapper = userControllerModelListUserModelListMapper;
		this.accountControllerModelAccountModelMapper = accountControllerModelAccountModelMapper;
	}

	@POST
	@Timed
	public UserControllerModel create(@Valid UserControllerModel user) {
		UserModel userModel = userModelUserControllerModelMapper.map(user);
		UserModel userModelResponse = userService.saveUser(userModel);
		return userControllerModelUserModelMapper.map(userModelResponse);
	}

	@GET
	@Timed
	public List<UserControllerModel> getAll() {
		List<UserModel> userList = userService.getAllUsers();
		return userControllerModelListUserModelListMapper.map(userList);
	}

	@GET
	@Timed
	@Path("/{username}")
	public UserControllerModel get(@PathParam("username") String username) {
		UserModel userModel = userService.getUser(username);
		return userControllerModelUserModelMapper.map(userModel);
	}

	@DELETE
	@Timed
	@Path("/{id}")
	public void delete(@PathParam("id") int id) {
		userService.deleteUser(id);
	}

	@PUT
	@Timed
	@Path("/{id}")
	public UserControllerModel update(@PathParam("id") int id, @Valid UserControllerModel userControllerModel) {
		UserModel userModel = userModelUserControllerModelMapper.map(userControllerModel);
		UserModel userModelResponse = userService.updateUser(id, userModel);
		return userControllerModelUserModelMapper.map(userModelResponse);
	}

	@GET
	@Timed
	@Path("/{username}/account")
	public AccountControllerModel account(@PathParam("username") @NotEmpty String username) {
		AccountModel accountModel = userService.account(username, accountRepository);
		return accountControllerModelAccountModelMapper.map(accountModel);
	}

}
