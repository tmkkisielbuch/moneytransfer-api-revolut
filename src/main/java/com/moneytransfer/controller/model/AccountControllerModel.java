package com.moneytransfer.controller.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

import io.dropwizard.validation.OneOf;

public class AccountControllerModel {
	private int id;

	@NotEmpty
	@Size(min = 2, max = 40, message = "Username can be between 2 and 20")
	private String username;

	@NotNull
	private Double balanceAmount;

	@NotEmpty
	@OneOf({"TRY","EUR", "USD"})
	private String currency;

	public AccountControllerModel() {
	}

	public AccountControllerModel(String username, Double balanceAmount, String currency) {
		super();
		this.username = username;
		this.balanceAmount = balanceAmount;
		this.currency = currency;
	}

	public AccountControllerModel(int id, String username, Double balanceAmount, String currency) {
		this.id = id;
		this.username = username;
		this.balanceAmount = balanceAmount;
		this.currency = currency;
	}

	@JsonProperty
	public int getId() {
		return id;
	}

	@JsonProperty
	public String getUsername() {
		return username;
	}

	@JsonProperty
	public Double getBalanceAmount() {
		return balanceAmount;
	}

	@JsonProperty
	public String getCurrency() {
		return currency;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		AccountControllerModel that = (AccountControllerModel) o;

		return Objects.equal(this.id, that.id) && Objects.equal(this.username, that.username)
				&& Objects.equal(this.balanceAmount, that.balanceAmount) && Objects.equal(this.currency, that.currency);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id, username, balanceAmount, currency);
	}
}
