package com.moneytransfer.controller;

import javax.validation.Valid;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.skife.jdbi.v2.sqlobject.Transaction;

import com.codahale.metrics.annotation.Timed;
import com.moneytransfer.controller.mapper.MoneyTransferModelMoneyTransferControllerModelMapper;
import com.moneytransfer.controller.model.MoneyTransferControllerModel;
import com.moneytransfer.domain.implementation.MoneyTransactionService;
import com.moneytransfer.domain.model.MoneyTransferModel;
import com.moneytransfer.infrastructure.repository.IAccountRepository;

@Path("/api/transfers")
@Produces(MediaType.APPLICATION_JSON)
public class MoneyTransactionController {

	IAccountRepository accountRepository;
	MoneyTransactionService transferService;
	MoneyTransferModelMoneyTransferControllerModelMapper moneyTransferModelMoneyTransferControllerModelMapper;

	public MoneyTransactionController(IAccountRepository accountRepository,
			MoneyTransactionService transferService,
			MoneyTransferModelMoneyTransferControllerModelMapper moneyTransferModelMoneyTransferControllerModelMapper) {
		super();
		this.accountRepository = accountRepository;
		transferService = new MoneyTransactionService(accountRepository);
		this.transferService = transferService;
		this.moneyTransferModelMoneyTransferControllerModelMapper = moneyTransferModelMoneyTransferControllerModelMapper;
	}

	@POST
	@Timed
	@Transaction
	public void moneyTransfer(@Valid MoneyTransferControllerModel transferControllerModel) {

		MoneyTransferModel transferModel = moneyTransferModelMoneyTransferControllerModelMapper
				.map(transferControllerModel);
		transferService.transfer(transferModel);
	}
}
