package com.moneytransfer.controller.mapper;

import com.moneytransfer.controller.model.CurrencyAmountControllerModel;
import com.moneytransfer.domain.model.CurrencyAmountModel;
import com.moneytransfer.domain.utils.IMapper;

public class CurrencyAmountControllerModelCurrencyAmountModelMapper
		implements IMapper<CurrencyAmountControllerModel, CurrencyAmountModel> {

	@Override
	public CurrencyAmountControllerModel map(CurrencyAmountModel model, Object... params) {
		CurrencyAmountControllerModel currControllerModel = new CurrencyAmountControllerModel();
		if (model != null) {
			currControllerModel = new CurrencyAmountControllerModel(model.getAmount(), model.getCurrency());
		}
		return currControllerModel;
	}

}
