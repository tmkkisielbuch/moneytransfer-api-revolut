package com.moneytransfer.controller.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.moneytransfer.controller.model.UserControllerModel;
import com.moneytransfer.domain.model.UserModel;
import com.moneytransfer.domain.utils.IMapper;

public class UserControllerModelListUserModelListMapper implements IMapper<List<UserControllerModel>, List<UserModel>> {

	@Override
	public List<UserControllerModel> map(List<UserModel> model, Object... params) {

		List<UserControllerModel> users = model.stream().map(temp -> {
			UserControllerModel user = new UserControllerModel(temp.getId(), temp.getUsername(), temp.getEmail());
			return user;
		}).collect(Collectors.toList());

		return users;
	}

}
