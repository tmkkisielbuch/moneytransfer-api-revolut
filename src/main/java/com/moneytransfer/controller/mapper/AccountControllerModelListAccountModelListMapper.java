package com.moneytransfer.controller.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.moneytransfer.controller.model.AccountControllerModel;
import com.moneytransfer.domain.model.AccountModel;
import com.moneytransfer.domain.utils.IMapper;

public class AccountControllerModelListAccountModelListMapper
		implements IMapper<List<AccountControllerModel>, List<AccountModel>> {

	@Override
	public List<AccountControllerModel> map(List<AccountModel> model, Object... params) {

		List<AccountControllerModel> accounts = model.stream().map(temp -> {
			AccountControllerModel account = new AccountControllerModel(temp.getId(), temp.getUsername(),
					temp.getBalanceAmount(), temp.getCurrency());
			return account;
		}).collect(Collectors.toList());
		
		return accounts;
	}

}
