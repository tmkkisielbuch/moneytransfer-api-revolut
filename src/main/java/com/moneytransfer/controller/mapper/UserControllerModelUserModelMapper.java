package com.moneytransfer.controller.mapper;

import com.moneytransfer.controller.model.UserControllerModel;
import com.moneytransfer.domain.model.UserModel;
import com.moneytransfer.domain.utils.IMapper;

public class UserControllerModelUserModelMapper implements IMapper<UserControllerModel, UserModel> {

	@Override
	public UserControllerModel map(UserModel model, Object... params) {
		UserControllerModel user = new UserControllerModel();
		if (model != null) {
			user = new UserControllerModel(model.getId(), model.getUsername(), model.getEmail());
		}
		return user;
	}

}
