package com.moneytransfer.controller.mapper;

import com.moneytransfer.controller.model.MoneyTransferControllerModel;
import com.moneytransfer.domain.model.MoneyTransferModel;
import com.moneytransfer.domain.utils.IMapper;

public class MoneyTransferModelMoneyTransferControllerModelMapper
		implements IMapper<MoneyTransferModel, MoneyTransferControllerModel> {

	@Override
	public MoneyTransferModel map(MoneyTransferControllerModel model, Object... params) {
		MoneyTransferModel transferModel = new MoneyTransferModel();
		if (model != null) {
			transferModel.setAmount(model.getAmount());
			transferModel.setCurrency(model.getCurrency());
			transferModel.setDestinationAccountId(model.getDestinationAccountId());
			transferModel.setSourceAccountId(model.getSourceAccountId());

		}
		return transferModel;
	}

}
