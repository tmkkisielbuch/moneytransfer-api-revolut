package com.moneytransfer.controller.mapper;

import com.moneytransfer.controller.model.AccountControllerModel;
import com.moneytransfer.domain.model.AccountModel;
import com.moneytransfer.domain.utils.IMapper;

public class AccountControllerModelAccountModelMapper implements IMapper<AccountControllerModel, AccountModel> {

	@Override
	public AccountControllerModel map(AccountModel model, Object... params) {
		AccountControllerModel acControllerModel = new AccountControllerModel();
		if (model != null) {
			acControllerModel = new AccountControllerModel(model.getId(), model.getUsername(), model.getBalanceAmount(),
					model.getCurrency());
		}
		return acControllerModel;
	}

}
