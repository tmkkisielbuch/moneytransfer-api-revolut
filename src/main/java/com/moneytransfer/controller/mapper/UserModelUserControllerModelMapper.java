package com.moneytransfer.controller.mapper;

import com.moneytransfer.controller.model.UserControllerModel;
import com.moneytransfer.domain.model.UserModel;
import com.moneytransfer.domain.utils.IMapper;

public class UserModelUserControllerModelMapper implements IMapper<UserModel, UserControllerModel> {

	@Override
	public UserModel map(UserControllerModel model, Object... params) {

		UserModel user = new UserModel();
		if (model != null) {
			user.setEmail(model.getEmail());
			user.setUsername(model.getUsername());
		}
		return user;
	}

}
