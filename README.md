Backend Test

Design and implement a RESTful API (including data model and the backing implementation)
for money transfers between accounts.
Explicit requirements:
1. You can use Java, Scala or Kotlin.
2. Keep it simple and to the point (e.g. no need to implement any authentication).
3. Assume the API is invoked by multiple systems and services on behalf of end users.
4. You can use frameworks/libraries if you like (except Spring), but don't forget about
requirement #2 – keep it simple and avoid heavy frameworks.
5. The datastore should run in-memory for the sake of this test.
6. The final result should be executable as a standalone program (should not require
a pre-installed container/server).
7. Demonstrate with tests that the API works as expected.
Implicit requirements:
1. The code produced by you is expected to be of high quality.
2. There are no detailed requirements, use common sense.

Using Techonologies :
1.	Java 8
2.	H2 in-memory Database
3.	Dropwizard framework
4.	Junit
5.	Mockito


Instalation
Method 1:
1.	mvn package
2.	java -jar target/moneytransfer-api-1.0.0-SNAPSHOT.jar server moneytransfer-api.yml

Method 2:
Eclipse
1.	Open your project’s Run Configurations menu by selecting Run -> Run Configurations.
2.	Create a new Java Application launch configuration.
3.	On the main tab choose a name for the configuration and specify the main class. com.moneytransfer.App
4.	On the arguments tab add server moneytransfer-api.yml to the program arguments text field.
5.	Click the run button or press Ctrl+F11 to run your Dropwizard application

IntelliJ IDEA
1.	Open Run/Debug Configuration menu by selecting Run -> Edit Configurations… from the menu.
2.	Add a new configuration by pressing Alt+Insert or clicking the green plus sign in the top left corner.
3.	From the dropdown menu, select Application.
4.	Add a name to the configuration.
5.	Enter the fully qualified name of your main class. com.moneytransfer.App
6.	Enter server moneytransfer-api.yml to the Program arguments field. These are the same arguments you would pass to your application if you were running it from the command line (view image below).
7.	Press Shift+F10 or select Run -> Run <your run configuration name> from the menu to start your Dropwizard application

Endpoints
1. GET     http://localhost:8080/api/users  => get all users
2. POST    http://localhost:8080/api/users  =>  create a user
3. DELETE  http://localhost:8080/api/users /{id}  =>  delete a user by his id
4. PUT     http://localhost:8080/api/users /{id}  =>  update a user
5.  GET     http://localhost:8080/api/users /{username} =>get a user by username
6. GET     http://localhost:8080/api/users /{username}/account  => get a user account
7. GET     http://localhost:8080/api/accounts   => get all accounts
8.  POST    http://localhost:8080/api/accounts => add an account
9.  GET     http://localhost:8080/api/accounts/{id}  => get an account by id
10.  PUT     http://localhost:8080/api/accounts/{id}  =>  update an account
11. GET     http://localhost:8080/api/accounts/{id}/balance => get account balance
12. DELETE  http://localhost:8080/api/accounts/{username}  => delete an account by username
13. PUT     http://localhost:8080/api/accounts/{username}/withdraw => withdraw money from an account
14. POST    http://localhost:8080/api/transfers  => transfer money between accounts


Examples :
1.	Create a user
`Endpoint: POST Method http://localhost:8080/api/users `
```
Request
{"username": "omer erkan","email": "omer@gmail.com"}
Response
{
    "id": 2,
    "username": "omer erkan",
    "email": "omer@gmail.com"
}
```

2.	Get all users:
`Endpoint: GET Method http://localhost:8080/api/users `
```
Response
[
    {
        "id": 1,
        "username": "omer",
        "email": "omer@gmail.com"
    },
    {
        "id": 2,
        "username": "omer erkan",
        "email": "omer@gmail.com"
    }
]
```



3.	Delete  a user
`Endpoint: DELETE Method http://localhost:8080/api/users/1`

4.	Update a user
`Endpoint: PUT Method http://localhost:8080/api/users/1`
```
Request
{"username": "omer erkan","email": "omer@gmail.com"}
Response
{
    "id": 1,
    "username": "omer erkan",
    "email": "omer@gmail.com"
}
```


5.	Get a user by username

`Endpoint: GET Method http://localhost:8080/api/users/omer erkan`
```
Response
{
    "id": 1,
    "username": "omer erkan",
    "email": "omer@gmail.com"
}
```


6.	Create an account
`Endpoint: POST Method http://localhost:8080/api/accounts`
```
Request
{
  "username":"omere erkan",
  "balanceAmount":100.00,
  "currency":"EUR"
}
Response
{
    "id": 1,
    "username": "omere erkan",
    "balanceAmount": 100.0,
    "currency": "EUR"
}
```



7.	Get all account
`Endpoint: GET Method http://localhost:8080/api/accounts`
```
Response
[
    {
        "id": 1,
        "username": "omere erkan",
        "balanceAmount": 100.0,
        "currency": "EUR"
    }
]
```


8.	Get an account
`Endpoint: GET Method http://localhost:8080/api/accounts/1`
```
Response
{
    "id": 1,
    "username": "omere erkan",
    "balanceAmount": 100.0,
    "currency": "EUR"
}
```


9.	Get balance account

`Endpoint: GET Method http://localhost:8080/api/accounts/1/balanceAmount`
```
Response
{
    "amount": 100.0,
    "currency": "EUR"
}

```




10.	Withdraw money
`Endpoint: POST Method http://localhost:8080/api/accounts/omere erkan/withdraw`
```
Request
{"amount": 10,"currency": "EUR" }
```


11.	Transfer Money between accounts

Firstly we add another account
`Enpoints : POST Method http://localhost:8080/api/accounts`

```
Request
{
  "username":"erkan",
  "balanceAmount":60.00,
  "currency":"EUR"
}
Response
{
    "id": 2,
    "username": "erkan",
    "balanceAmount": 60.0,
    "currency": "EUR"
}
```

Secondly we are checking to see all accounts
`Endpoint: GET Method http://localhost:8080/api/accounts`
```
Response
[
    {
        "id": 1,
        "username": "omere erkan",
        "balanceAmount": 100.0,
        "currency": "EUR"
    },
    {
        "id": 2,
        "username": "erkan",
        "balanceAmount": 70.0,
        "currency": "EUR"
    }
]
```


Finally, we will transfer money from first account to second one
`Endpoint : POST Method http://localhost:8080/api/transfers`
```
Request 
{"amount":10.00,"currency":"EUR","sourceAccountId":"1","destinationAccountId":"2" }
```


Lastly, we are going to list all accounts again to see transfer

`Endpoint: GET Method http://localhost:8080/api/accounts`
```
Response
[
    {
        "id": 1,
        "username": "omere erkan",
        "balanceAmount": 90.0,
        "currency": "EUR"
    },
    {
        "id": 2,
        "username": "erkan",
        "balanceAmount": 80.0,
        "currency": "EUR"
    }
]
```


